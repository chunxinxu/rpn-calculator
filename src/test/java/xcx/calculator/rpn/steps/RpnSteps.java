package xcx.calculator.rpn.steps;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.springframework.stereotype.Component;

@Component
public class RpnSteps {

    @Given("stack is empty")
    public void givenStackIsEmpty() {
        // PENDING
    }

    @When("user input numbers 2 4 5")
    public void whenUserInputNumbers245() {
        // PENDING
    }

    @Then("stack output [2 4 5]")
    public void thenStackOutput245() {
        // PENDING
    }
}
