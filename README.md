# rpn-calculator
Airwallex

# RPNCalculator
CLI application in Spring Boot

# How to run
mvn spring-boot:run

# How to unit test
mvn test

# How to BDD tests: 
mvn integration-test
